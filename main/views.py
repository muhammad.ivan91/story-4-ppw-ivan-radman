from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse, JsonResponse
from .models import matkul
from .forms import tambahkan
from pip._vendor import requests
import json

def story1(request):
    return render(request, 'main/Story-1.html')
def home(request):
    return render(request, 'main/Story-3_page1.html')
def about(request):
    return render(request, 'main/Story-3_page2.html')
def riwayat_pendidikan(request):
    return render(request, 'main/Story-3_page3.html')
def matkuls(request):
    form = tambahkan()
    context = {'form':form}
    if request.method == 'POST':
        form = tambahkan(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/hasil')
    return render(request, 'main/matkul.html', context)
def hasil(request):
    result = matkul.objects.all()
    context = {'result':result}
    return render(request, 'main/hasil.html', context)
def hapus(request, pk):
    hapus = matkul.objects.get(id=pk)
    if request.method == 'POST':
        hapus.delete()
        return redirect('/hasil')
    context = {'del':hapus}
    return render(request, 'main/hapus.html', context)
def accordion(request):
    return render(request, 'main/accordion.html')
def ajax(request):
    response = {}
    return render(request, 'main/ajax.html', response)
def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)

    data = json.loads(ret.content)
    return JsonResponse(data,safe=False)