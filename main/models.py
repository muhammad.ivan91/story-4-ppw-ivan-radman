from django.db import models
from django.utils import timezone
from datetime import datetime, date

class matkul(models.Model):
    nama = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    sks = models.CharField(max_length=30)
    deskripsi = models.CharField(max_length=30)
    tahun = models.CharField(max_length=30)
    kelas = models.CharField(max_length=30)