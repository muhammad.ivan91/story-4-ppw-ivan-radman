from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.story1, name='story1'),
    path('home', views.home, name='home'),
    path('about', views.about, name='about'),
    path('riwayat_pendidikan', views.riwayat_pendidikan, name='riwayat_pendidikan'),
    path('matkuls', views.matkuls, name='matkuls'),
    path('hasil', views.hasil, name='hasil'),
    path('hapus/<str:pk>/', views.hapus, name='hapus'),
    path('accordion', views.accordion, name='accordion'),
    path('ajax', views.ajax, name='ajax'),
    path('data/', views.data,  name='data'),
]
