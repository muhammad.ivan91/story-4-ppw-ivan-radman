from django import forms
from .models import Kegiatan, Peserta

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'

        labels = {
            'activity' : 'Kegiatan'
        }

class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = '__all__'

        labels = {
            'participant' : 'Peserta'
        }
