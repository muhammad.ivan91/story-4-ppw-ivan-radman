from django.shortcuts import render, redirect
from .forms import KegiatanForm, PesertaForm
from .models import Kegiatan, Peserta

# Create your views here.

def isikegiatan(request):
    form = KegiatanForm()

    if request.method == "POST":
        form = KegiatanForm(request.POST)
        if form.is_valid():
            post = form.save(commit=True)
            post.save()
            return redirect('/daftarkegiatan')
    else:
        form = KegiatanForm()

    return render(request, 'isikegiatan.html', {'form' : form})

def daftarkegiatan(request):
    daftar = Kegiatan.objects.all()

    context = {'obj': daftar}
    return render(request, 'daftarkegiatan.html', context)

def peserta(request, id):
    daftar = Kegiatan.objects.get(id=id)
    peserta_form = PesertaForm(request.POST, instance=daftar)

    if peserta_form.is_valid():
        submitted_data = Peserta()
        submitted_data.participant = peserta_form.cleaned_data['participant']
        submitted_data.act = peserta_form.cleaned_data['act']
        submitted_data.save()
        return redirect('/daftarkegiatan')

    else:
        peserta_form = PesertaForm()

    context = {'daftar': daftar, 'peserta_form': peserta_form}
    return render(request, 'peserta.html', context)
