from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from selenium import webdriver
from django.urls import resolve, reverse
from .views import isikegiatan, daftarkegiatan, peserta
from .models import Kegiatan, Peserta
from .forms import KegiatanForm, PesertaForm
from .urls import urlpatterns

# Create your tests here.

class Story6_Test(TestCase):
    
    def setUp(self):
        Kegiatan.objects.create(activity="BEM")
        Kegiatan.objects.create(activity="FUKI")
        Kegiatan.objects.create(activity="DPM")

    def test_check_model_create_kegiatan(self):
        Kegiatan.objects.create(activity="BEM")
        amount = Kegiatan.objects.all().count()
        self.assertEqual(amount, 4)

    def test_model_return_kegiatan(self):
        Kegiatan.objects.create(activity="BEM")
        hasil = Kegiatan.objects.get(id=1)
        self.assertEqual(str(hasil), "BEM")

    def test_model_return_peserta(self):
        Kegiatan.objects.create(activity="BEM")
        obj1 = Kegiatan.objects.get(id=1)
        
        Peserta.objects.create(act=obj1, participant="Ivan")
        hasil = Peserta.objects.get(id=1)
        self.assertEqual(str(hasil), "Ivan")

#Isi Kegiatan
    def test_url_isikegiatan(self):
        response = Client().get('/isikegiatan')
        self.assertEqual(response.status_code, 200)

    def test_isikegiatan_template(self):
        response = Client().get('/isikegiatan')
        self.assertTemplateUsed(response, 'isikegiatan.html')

    def test_isikegiatan_func(self):
        found = resolve('/isikegiatan')
        self.assertEqual(found.func, isikegiatan)
    
    def test_view_dalam_template_isikegiatan(self):
        response = Client().get('/isikegiatan')  
        isi_view = response.content.decode('utf8')
        self.assertIn('<form method="POST">', isi_view)
        self.assertIn("Silakan isi kegiatan di bawah ini !", isi_view)   
        self.assertIn('<input class="button" type="submit" value="Tambahkan">', isi_view)


#Daftar Kegiatan
    def test_url_daftarkegiatan(self):
        response = Client().get('/daftarkegiatan')
        self.assertEqual(response.status_code, 200)

    def test_daftarkegiatan_template(self):
        response = Client().get('/daftarkegiatan')
        self.assertTemplateUsed(response, 'daftarkegiatan.html')

    def test_daftarkegiatan_func(self):
        found = resolve('/daftarkegiatan')
        self.assertEqual(found.func, daftarkegiatan)
    
    def test_view_dalam_template_daftarkegiatan(self):
        response = Client().get('/daftarkegiatan')  
        isi_view = response.content.decode('utf8')
        self.assertIn('<form method="POST">', isi_view)
        self.assertIn("Daftar Kegiatan", isi_view)

    
#Peserta
    def test_url_peserta(self):
        response = Client().get('/peserta/1')
        self.assertEqual(response.status_code, 200)
    
    def test_peserta_template(self):
        response = Client().get('/peserta/1')
        self.assertTemplateUsed(response, 'peserta.html')

    def test_peserta_func(self):
        found = resolve('/peserta/1')
        self.assertEqual(found.func, peserta)
    
    def test_view_dalam_template_peserta(self):
        response = Client().get('/peserta/1')  
        isi_view = response.content.decode('utf8')
        self.assertIn('<form method="POST">', isi_view)
        self.assertIn("Silakan isi nama untuk mendaftar kegiatan di bawah ini !", isi_view)
        self.assertIn('<input class="button" type="submit" value="Submit">', isi_view)
